import getopt
import pickle
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.metrics import average_precision_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier


def main():

    if len(sys.argv[1:]) != 1:
        print('USAGE - python3 make-model-c45.py <dataset.pkl> ')
        return

    input_file_name = sys.argv[1]
    input_file = open(input_file_name, 'rb')
    df = pickle.load(input_file)
    input_file.close()

    # --------------------- #
    #  Data Pre-processing  #
    # --------------------- #
    class_label = 'is_safe'
    feature_labels = [x for x in df.columns.values if x != class_label]

    feature_scaler = MinMaxScaler(feature_range=(0, 1))

    X = df[feature_labels].values
    X = feature_scaler.fit_transform(X)  

    y = np.ravel(df[[class_label]].values)

    # ---------------- #
    #  Model Creation  #
    # ---------------- #
    seed = 0
    cv = StratifiedKFold(n_splits=5) # create a 5-fold cross validator

    classifiers = []
    
    classifiers.append(('C4.5',DecisionTreeClassifier(criterion='entropy', max_depth=9, random_state=seed)))
    classifiers.append(('Random Forest', RandomForestClassifier(n_estimators=70, max_depth=15,random_state=0)))
    classifiers.append(('k-Nearest-Neighbour', KNeighborsClassifier(n_neighbors=14)))
    classifiers.append(('Multilayer Perceptron', MLPClassifier(hidden_layer_sizes=(50,100,75),alpha=0.00001, random_state=seed)))
    
    for name, clf in classifiers:

        TRPs = [] # true positive rate
        FPRs = [] # false positive rate
        AUCs = [] # area under curve
        pos_PRCs = [] # positiveprecision
        pos_RCLs = [] # positiverecall
        neg_PRCs = [] # negative precision
        neg_RCLs = [] # negative recall

        for train, test in cv.split(X, y):

            # fit the model over the training set
            clf.fit(X[train], y[train])
            output_class = clf.predict(X[test])
            output_proba = clf.predict_proba(X[test])
            
            # compute the precision metric
            pos_PRCs.append(precision_score(y[test], output_class, pos_label=1))
            neg_PRCs.append(precision_score(y[test], output_class, pos_label=0))
            
            # compute the recall metric
            pos_RCLs.append(recall_score(y[test], output_class, pos_label=1))
            neg_RCLs.append(recall_score(y[test], output_class, pos_label=0))

            # Compute ROC curve and area the curve
            fpr, tpr, thresholds = roc_curve(y[test], output_proba[:, 1])
            roc_auc = auc(fpr, tpr)
            AUCs.append(roc_auc)

        print('\n' + name + '\n' + '-'*75)
        print('POSITVE PRECISION:  ' + "{:10.4f}".format(np.mean(pos_PRCs)))
        print('NEGATIVE PRECISION: ' + "{:10.4f}".format(np.mean(neg_PRCs)))
        print('POSITIVE RECALL:' + "{:10.4f}".format(np.mean(pos_RCLs)))
        print('NEGATIVE RECALL:' + "{:10.4f}".format(np.mean(neg_RCLs)))
        print('AREA UNDER CURVE:' + "{:10.4f}".format(np.mean(AUCs)))
        print('\n')

if __name__ == "__main__":
    main()

