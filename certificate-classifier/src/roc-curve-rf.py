import datetime
import getopt
import pickle
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from itertools import cycle

from scipy import interp

from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_curve, auc

from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import BernoulliNB, GaussianNB, MultinomialNB
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier

from sty import fg, bg, ef, rs

def main():

    if len(sys.argv[1:]) != 2:
        log(fg.red, 'USAGE - python3 make-model-random-forest.py <dataset.pkl> <output-file.pkl>')
        return

    input_file_name = sys.argv[1]
    output_file_name = sys.argv[2]

    input_file = open(input_file_name, 'rb')
    df = pickle.load(input_file)
    
    # --------------------- #
    #  Data Pre-processing  #
    # --------------------- #
    class_label = 'is_safe'
    feature_labels = [x for x in df.columns.values if x != class_label]

    feature_scaler = MinMaxScaler(feature_range=(0, 1))

    X = df[feature_labels].values
    X = feature_scaler.fit_transform(X)  

    y = np.ravel(df[[class_label]].values)
    
    cv = StratifiedKFold(n_splits=5) # create a 5-fold cross validator
    # classifier = RandomForestClassifier(n_estimators=500, random_state=0)
    # classifier = BernoulliNB(alpha=0.1)
    # classifier = GaussianNB()
    # classifier = MultinomialNB()
    # classifier = LinearSVC()#(probability=True)
    classifier = KNeighborsClassifier(n_neighbors=1)

    alphas = np.logspace(-10, 0, 5)

    names = []
    classifiers = []
    figures = []

    figure = plt.subplots()

    # for i in alphas:
    #     names.append('alpha ' + str(i) + ':')
    #     classifiers.append(MLPClassifier(alpha=i, random_state=1))
    #     figures.append(plt.subplots())

    # for name, classifier, figure in zip(names, classifiers, figures):
    axis = figure[1]
    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)
    i = 0

    for train, test in cv.split(X, y):
        probas_ = classifier.fit(X[train], y[train]).predict_proba(X[test])
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y[test], probas_[:, 1])
        tprs.append(interp(mean_fpr, fpr, tpr))
        tprs[-1][0] = 0.0
        roc_auc = auc(fpr, tpr)
        aucs.append(roc_auc)
        plt.plot(fpr, tpr, lw=1, alpha=0.3, label='ROC fold %d (AUC = %0.2f)' % (i, roc_auc))
        i += 1

        # mean_tpr = np.mean(tprs, axis=0)
        # mean_tpr[-1] = 1.0
        # mean_auc = auc(mean_fpr, mean_tpr)
        # std_auc = np.std(aucs)
        # axis.plot(mean_fpr, mean_tpr, color='b', label= name + r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc), lw=2, alpha=.8)

        # axis.set_xlim([0, 1.00])
        # axis.set_ylim([0, 1.00])
        # axis.set_xlabel('False Positive Rate')
        # axis.set_ylabel('True Positive Rate')
        # axis.set_title('Receiver Operating Characteristic (ROC)')
        # axis.legend(loc="lower right")
    
    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs)
    plt.plot(mean_fpr, mean_tpr, color='b', label= r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc), lw=2, alpha=.8)

    
    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2, label=r'$\pm$ 1 std. dev.')

    plt.xlim([0, 1.00])
    plt.ylim([0, 1.00])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic (ROC)')
    plt.legend(loc="lower right")
    
    plt.show()

def log(color, msg):
    log = color
    log += '[' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ']: '
    log += msg
    log += fg.rs
    print(log)

if __name__ == "__main__":
    main()