import sys, getopt
import socket, ssl
import OpenSSL
import datetime
import pickle
import threading

from RawCertificate import RawCertificate
from sty import fg, bg, ef, rs
import multiprocessing as mp 

def main():
    a = datetime.datetime.now()
    if len(sys.argv[1:]) != 3:
        log(fg.red, 'USAGE - python3 get-cerificates.py <quantity> <input-file.pkl> <output-file.pkl>')
        return

    qty = int(sys.argv[1])
    input_file_name = sys.argv[2]
    output_file_name = sys.argv[3]
    input_file = open(input_file_name, 'rb')
    output_file = open(output_file_name, 'wb')

    domains = pickle.load(input_file)
    log(fg.green, 'Collecting from ' + str(len(domains)) + ' domains')
    certificates = []

    a = datetime.datetime.now()

    pool = mp.Pool(50) 
    results = pool.map(collect_certificate, domains[0:qty])

    pool.close() 
    pool.join()
    
    b = datetime.datetime.now()
    delta = b - a

    certificates = list(filter(None, results))
    pickle.dump(certificates, output_file)

    log(fg.green, str(len(certificates)) + ' certificates written to ' + output_file_name +' in ' + str(delta.total_seconds())+ 'sec') 

    input_file.close()
    output_file.close()


def collect_certificate(domain): 
    port = 443 # port reserved for secure comm. over TLS

    try:
        certificate = RawCertificate(domain, ssl.get_server_certificate((domain, port)))
        # log(fg.green, 'PASS')

    except:
        certificate =  None
        # log(fg.red, 'FAIL')
    
    return certificate

def log(color, msg):
    log = color
    log += '[' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ']: '
    log += msg
    log += fg.rs
    print(log)

if __name__ == "__main__":
    main()

