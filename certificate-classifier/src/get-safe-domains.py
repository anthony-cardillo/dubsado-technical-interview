import datetime
import getopt
import json
import http.client
import pickle
import sys

from sty import fg, bg, ef, rs
from urllib.parse import urlparse

def main():
    if len(sys.argv[1:]) != 2:
        log(fg.red, 'USAGE - python3 get-safe-domains.py <input-file.json> <output-file.pkl>')
        return

    input_file_name = sys.argv[1]
    output_file_name = sys.argv[2]
    input_file = open(input_file_name)
    output_file = open(output_file_name, 'wb')

    raw_data = json.load(input_file)
    raw_domains = []
    domains = []

    for record in raw_data:   
        if record['alexa_rank'] is not None:
            domain = record['domain']
            raw_domains.append(domain)

    domains = list(set(raw_domains))
    pickle.dump(domains, output_file)
    log(fg.green, str(len(domains)) + ' domains written to ' + output_file_name)

    input_file.close()
    output_file.close()
    
def log(color, msg):
    log = color
    log += '[' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ']: '
    log += msg
    log += fg.rs
    print(log)

if __name__ == "__main__":
    main()