from datetime import datetime

class RawCertificate:

    def __init__(self, _domain, _x509):
        self.collected_on = datetime.now()
        self.domain = _domain
        self.x509 = _x509