from fuzzywuzzy import fuzz
from datetime import datetime
import pandas as pd
import OpenSSL

class FeatureVector:

    def __init__(self, certificate):

        x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, certificate.x509)
        self.features = {}

        # Time-Based (TB) Features
        not_before = get_feature_TB_not_before(x509)
        not_after = get_feature_TB_not_after(x509)
        
        self.features['has_expired'] = get_feature_TB_has_expired(x509)
        self.features['valid_duration'] = get_feature_TB_valid_duration(not_before, not_after)
        self.features['since_created' ] = get_feature_TB_since_created(not_before, certificate.collected_on)
        self.features['until_expired' ] = get_feature_TB_until_expired(not_after, certificate.collected_on)

        # ISSUER/SUBJECT/DOMAIN (DN) Features
        issuer = get_feature_DN_issuer(x509)
        subject = get_feature_DN_subject(x509)

        self.features['match_issuer_cn_o'] = get_feature_DN_match_issuer_cn_o(issuer)
        self.features['match_issuer_o_ou'] = get_feature_DN_match_issuer_o_ou(issuer)

        self.features['match_domain_issuer'] = get_feature_DN_match_domain_issuer(certificate.domain, issuer)
        self.features['match_domain_subject'] = get_feature_DN_match_domain_subject(certificate.domain, subject)
        self.features['match_subject_issuer'] = get_feature_DN_match_subject_issuer(subject, issuer)

        # Signature Algorithm (SA) Features
        signature_algorithm = get_feature_signature_algorithm(x509)

        self.features['is_hash_SHA1'] = get_feature_SA_is_hash_SHA1(signature_algorithm) 
        self.features['is_hash_SHA256'] = get_feature_SA_is_hash_SHA256(signature_algorithm) 
        self.features['is_hash_SHA384'] = get_feature_SA_is_hash_SHA384(signature_algorithm) 
        self.features['is_hash_SHA512'] = get_feature_SA_is_hash_SHA512(signature_algorithm) 
        self.features['is_hash_MD2'] = get_feature_SA_is_hash_MD2(signature_algorithm) 
        self.features['is_hash_MD5'] = get_feature_SA_is_hash_MD5(signature_algorithm) 
        self.features['is_encrpyt_RSA'] = get_feature_SA_is_encrpyt_RSA(signature_algorithm) 
        self.features['is_encrpyt_ECDSA'] = get_feature_SA_is_encrpyt_ECDSA(signature_algorithm) 
        self.features['is_encrpyt_DSA'] = get_feature_SA_is_encrpyt_DSA(signature_algorithm) 

        # Miscellaneous (MC) Features
        self.features['version'] = get_feature_MC_version(x509)
        self.features['serial_num_length'] = get_feature_MC_serial_num_length(x509)
        
        public_key = get_feature_MC_public_key(x509)

        self.features['key_length'] = get_feature_MC_key_length(public_key) 
        self.features['key_type'] = get_feature_MC_key_type(public_key)

        # Extension (EX) Features
        extensions = get_feature_EX_extensions(x509)
        extension_names = get_feature_EX_extension_names(extensions)

        self.features['total_ext_count'] = get_feature_EX_total_ext_count(x509)
        self.features['critical_ext_count'] = get_feature_EX_critical_ext_count(extensions)
        self.features['noncritical_ext_count'] = get_feature_EX_noncritical_ext_count(self.features['total_ext_count'], self.features['critical_ext_count'])

        self.features['has_ext_authority_key_identifier'] = get_feature_EX_has_ext_authority_key_identifier(extension_names)
        self.features['has_ext_subject_key_identifier'] = get_feature_EX_has_ext_subject_key_identifier(extension_names)
        self.features['has_ext_key_usage'] = get_feature_EX_has_ext_key_usage(extension_names)
        self.features['has_ext_certificate_policies'] = get_feature_EX_has_ext_certificate_policies(extension_names)
        self.features['has_ext_subject_alt_name'] = get_feature_EX_has_ext_subject_alt_name(extension_names)
        self.features['has_ext_issuer_alt_name'] = get_feature_EX_has_ext_issuer_alt_name(extension_names)
        self.features['has_ext_subject_directory_attributes'] = get_feature_EX_has_ext_subject_directory_attributes(extension_names)
        self.features['has_ext_basic_constraints'] = get_feature_EX_has_ext_basic_constraints(extension_names)
        self.features['has_ext_is_extended_validation'] = get_feature_EX_has_ext_is_extended_validation(extension_names)
        self.features['has_ext_extendeded_key_usage'] = get_feature_EX_has_ext_extended_key_usage(extension_names)
        self.features['has_ext_crl_distribution_points'] = get_feature_EX_has_ext_crl_distribution_points(extension_names)
        self.features['has_ext_freshest_CRL'] = get_feature_EX_has_ext_freshest_CRL(extension_names)
        self.features['has_ext_authority_info_access'] = get_feature_EX_has_ext_authority_info_access(extension_names)


    def log(self):
        print('FEATURE VECTOR (' + str(len(self.features)) + ')')
        print('-' * 75)
        for x in self.features.keys():
            print( '  > ' + x.ljust(45) + str(self.features[x]))
   
    def get_feature_labels(self):
        return self.features.keys()

    def get_feature_list(self, feature_labels):
        feature_list = []
        for feature_label in feature_labels:
            feature_list.append(self.features[feature_label])
        return feature_list

        
# Time-Based (TB) Features
def get_feature_TB_not_before(certificate):
    # return pd.to_datetime(certificate.get_notBefore().decode('ascii'))
    return datetime.strptime(certificate.get_notBefore().decode('ascii'),'%Y%m%d%H%M%SZ')

def get_feature_TB_has_expired(certificate):
    return int(certificate.has_expired())

def get_feature_TB_not_after(certificate):
    # return pd.to_datetime(certificate.get_notAfter().decode('ascii'))
    return datetime.strptime(certificate.get_notAfter().decode('ascii'),'%Y%m%d%H%M%SZ')

def get_feature_TB_valid_duration(not_before, not_after):
    return max(0, (not_after - not_before).days)

def get_feature_TB_since_created(not_before, collected_on):
    return max(0, (collected_on - not_before).days)

def get_feature_TB_until_expired(not_after, collected_on):
    return max(0, (not_after - collected_on).days)


# ISSUER/SUBJECT/DOMAIN (DN) Features
def get_feature_DN_issuer(certificate):
    return certificate.get_issuer()

def get_feature_DN_subject(certificate):
    return certificate.get_subject()

def get_feature_DN_match_issuer_cn_o(issuer):
    return fuzz.token_set_ratio(issuer.commonName, issuer.organizationName)

def get_feature_DN_match_issuer_o_ou(issuer):
    return fuzz.token_set_ratio(issuer.organizationalUnitName, issuer.organizationName)

def get_feature_DN_match_domain_issuer(domain, issuer):
    return fuzz.ratio(domain, issuer.commonName)

def get_feature_DN_match_domain_subject(domain, subject):
    return fuzz.ratio(domain, subject.commonName)

def get_feature_DN_match_subject_issuer(subject, issuer):
    return fuzz.ratio(subject.commonName, issuer.commonName)


# Signature Algorithm (SA) Features
def get_feature_signature_algorithm(certificate):
    return certificate.get_signature_algorithm().decode("ascii")

def get_feature_SA_is_hash_SHA1(algorithm_name):
    return 1 if ('sha1' in algorithm_name) else 0

def get_feature_SA_is_hash_SHA256(algorithm_name):
    return 1 if ('sha256' in algorithm_name) else 0

def get_feature_SA_is_hash_SHA384(algorithm_name):
    return 1 if ('sha384' in algorithm_name) else 0

def get_feature_SA_is_hash_SHA512(algorithm_name):
    return 1 if ('sha512' in algorithm_name) else 0

def get_feature_SA_is_hash_MD2(algorithm_name):
    return 1 if ('MD2' in algorithm_name) else 0

def get_feature_SA_is_hash_MD5(algorithm_name):
    return 1 if ('MD5' in algorithm_name) else 0

def get_feature_SA_is_encrpyt_RSA(algorithm_name):
    return 1 if ('RSA' in algorithm_name) else 0

def get_feature_SA_is_encrpyt_ECDSA(algorithm_name):
    return 1 if ('ECDSA' in algorithm_name) else 0

def get_feature_SA_is_encrpyt_DSA(algorithm_name):
    return 1 if (('DSA' in algorithm_name) and not ('ECDSA' in algorithm_name)) else 0


# Miscellaneous (MC) Features
def get_feature_MC_version(certificate):
    return certificate.get_version()

def get_feature_MC_serial_num_length(certificate):
    return len(str(certificate.get_serial_number()))

def get_feature_MC_public_key(certificate):
    return certificate.get_pubkey()

def get_feature_MC_key_length(public_key):
    return public_key.bits()

def get_feature_MC_key_type(public_key):
    return public_key.type()


# Extension (EX) Features
def get_feature_EX_extensions(certificate):
    extensions = []
    for j in range(0, certificate.get_extension_count()):
        extensions.append(certificate.get_extension(j))
    return extensions

def get_feature_EX_total_ext_count(certificate):
    return certificate.get_extension_count()

def get_feature_EX_critical_ext_count(extensions):
    return sum(ext.get_critical() for ext in extensions)

def get_feature_EX_noncritical_ext_count(total_count, critical_count):
    return total_count - critical_count

def get_feature_EX_extension_names(extensions):
    names = []
    for ext in extensions:
        names.append(ext.get_short_name().decode('ascii'))
    return names

def get_feature_EX_has_ext_authority_key_identifier(extension_names):
    return 1 if ('authorityKeyIdentifier' in extension_names) else 0

def get_feature_EX_has_ext_subject_key_identifier(extension_names):
    return 1 if ('subjectKeyIdentifier' in extension_names) else 0

def get_feature_EX_has_ext_key_usage(extension_names):
    return 1 if ('keyUsage' in extension_names) else 0

def get_feature_EX_has_ext_certificate_policies(extension_names):
    return 1 if ('certificatePolicies' in extension_names) else 0

def get_feature_EX_has_ext_subject_alt_name(extension_names):
    return 1 if ('subjectAltName' in extension_names) else 0

def get_feature_EX_has_ext_issuer_alt_name(extension_names):
    return 1 if ('issuerAltName' in extension_names) else 0

def get_feature_EX_has_ext_subject_directory_attributes(extension_names):
    return 1 if ('subjectDirectoryAttributes' in extension_names) else 0

def get_feature_EX_has_ext_basic_constraints(extension_names):
    return 1 if ('basicConstraints' in extension_names) else 0

def get_feature_EX_has_ext_is_extended_validation(extension_names):
    return 1 if ('isExtendedValidation' in extension_names) else 0

def get_feature_EX_has_ext_extended_key_usage(extension_names):
    return 1 if ('extendedKeyUsage' in extension_names) else 0

def get_feature_EX_has_ext_crl_distribution_points(extension_names):
    return 1 if ('crlDistributionPoints' in extension_names) else 0

def get_feature_EX_has_ext_freshest_CRL(extension_names):
    return 1 if ('freshestCRL' in extension_names) else 0
    
def get_feature_EX_has_ext_authority_info_access(extension_names):
    return 1 if ('authorityInfoAccess' in extension_names) else 0
