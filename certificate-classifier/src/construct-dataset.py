import datetime
import getopt
import pickle
import sys

import pandas as pd

from RawCertificate import RawCertificate
from FeatureVector import FeatureVector
from sty import fg, bg, ef, rs

def main():
    if len(sys.argv[1:]) != 3:
        log(fg.red, 'USAGE - python3 construct-dataset.py <pos-input-file.pkl> <neg-input-file.pkl> <output-file.pkl>')
        return

    pos_input_file_name = sys.argv[1]
    neg_input_file_name = sys.argv[2]
    output_file_name = sys.argv[3]

    pos_input_file = open(pos_input_file_name, 'rb')
    neg_input_file = open(neg_input_file_name, 'rb')
    output_file = open(output_file_name, 'wb')

    size = 6000

    safe_certificates = pickle.load(pos_input_file)
    log(fg.green, str(len(safe_certificates)) + ' SAFE certificates loaded')
    
    safe_FVs = [FeatureVector(x).features for x in safe_certificates[0:size]]
    log(fg.green, str(len(safe_FVs)) +' feature vectors generated')

    safe_df = pd.DataFrame(safe_FVs)
    safe_df['is_safe'] = 1

    # print(safe_df.head())
    # print('\n')
    
    evil_certificates = pickle.load(neg_input_file)
    log(fg.green, str(len(evil_certificates)) + ' EVIL certificates loaded')

    evil_FVs = [FeatureVector(x).features for x in evil_certificates[0:size]]
    log(fg.green, str(len(evil_FVs)) + ' feature vectors generated')
    
    evil_df = pd.DataFrame(evil_FVs)
    evil_df['is_safe'] = 0

    # print(evil_df.head())
    # print('\n')

    dataset = safe_df.append(evil_df, ignore_index=True)
    pickle.dump(dataset, output_file)

    log(fg.green, 'COMPLETE - dataset generated with ' + str(dataset.shape[0]) + ' vectors of ' + str(dataset.shape[1]) + ' features')
    
    # print(dataset.head())
    # print('\n')
    # print(dataset.tail())

    pos_input_file.close()
    neg_input_file.close()
    output_file.close()
    

def log(color, msg):
    log = color
    log += '[' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ']: '
    log += msg
    log += fg.rs
    print(log)

if __name__ == "__main__":
    main()