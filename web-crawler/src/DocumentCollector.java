import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class DocumentCollector implements Runnable {

    private BlockingQueue<Node> inputQueue = new LinkedBlockingQueue<>();

    private Network network;

    DocumentCollector(Network network) {
        this.network = network;
    }

    @Override
    public void run() {
        try {
            network.incrementActiveThreadCount();
            log("STARING COLLECTOR THREAD");
            while (!network.isFull()) {
                Node n = this.inputQueue.poll(20, TimeUnit.MILLISECONDS);
                if (n != null) {
                    n.collectDocument();
                    n.unlock();
                }
                Thread.yield();
            }
        } catch (Exception e) {
            log("Caught exception (" + e.getClass().getCanonicalName() + ") --> CLOSING THREAD");
            e.printStackTrace();
        } finally {
            log("CLOSING COLLECTOR THREAD");
            network.decrementActiveThreadCount();
        }
    }

    public void queueNode(Node node) {
        this.inputQueue.add(node);
    }

    public boolean hasNodes() {
        return !inputQueue.isEmpty();
    }

    private void log(String x) {
        System.out.println("[DC" + Thread.currentThread().getId() + "]: " + x);
    }
}

class DCTest {

    public static void main(String[] args) {

        String url = "https://whisperlab.org/";
        Network net = new Network();
        Node node = net.addNode(url);

        DocumentCollector dc = new DocumentCollector(net);
        Thread object = new Thread(dc);
        object.start();

        dc.queueNode(node);
    }
}


