import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class LinkScraper implements Runnable {

    private BlockingQueue<Node> inputQueue = new LinkedBlockingQueue<>();

    private Network network;

    LinkScraper(Network network) {
        this.network = network;
    }


    @Override
    public void run() {
        try {
            network.incrementActiveThreadCount();
            log("STARING SCRAPPING THREAD");
            while (!network.isFull()) {
                Node n = this.inputQueue.poll(20, TimeUnit.MILLISECONDS);
                if (n != null) {
                    n.scrapeLinks();
                    n.unlock();
                }
                Thread.yield();
            }
        } catch (Exception e) {
            log("Caught exception (" + e.getClass().getCanonicalName() + ") --> CLOSING THREAD");
            e.printStackTrace();
        } finally {
            log("CLOSING SCRAPPING THREAD");
            network.decrementActiveThreadCount();
        }

    }

    public void queueNode(Node node) {
        this.inputQueue.add(node);
    }

    public boolean hasNodes() {
        return !inputQueue.isEmpty();
    }

    private void log(String x) {
        System.out.println("[LS" + Thread.currentThread().getId() + "]: " + x);
    }

}

class LSTest {

    public static void main(String[] args) {

        String url = "https://whisperlab.org/";
        Network net = new Network();
        Node node = net.addNode(url);
        node.collectDocument();

        DocumentCollector dc = new DocumentCollector(net);
        LinkScraper ls = new LinkScraper(net);
        Thread object = new Thread(ls);
        object.start();

        ls.queueNode(node);
    }
}
