import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Node implements Serializable, Runnable {

    private Network network;
    private String url;
    private transient Document document = null;
    private List<Node> children =  null;

    private boolean isAvailable = true;
    private boolean isCollected = false;
    private boolean isValid = false;



    // WEB GRAPH PROPERTIES
    // ------------------------------------

    // 1. Hyperlink-Induced Topic Search
    public double authorityScore;
    public double hubScore;
    // 2. Google PageRank
    public Double pageRank;
    // 3. In-Out Degree
    public int inDegree;
    public int outDegree;



    Node(String url, Network network) {
        this.url = url;
        this.network = network;
    }

    public Network getNetwork() {return this.network;}

    public String getURL() {
        return this.url;
    }

    public Double getPageRank() {return pageRank;}

    public List<Node> getParents() {
        return this.network.allNodes.stream().filter(n -> n.hasChild(this)).collect(Collectors.toList());
    }

    public List<Node> getChildren() {
        return this.children;
    }

    public boolean hasChild(Node node) {
        if(children == null) return false;
        return this.children.contains(node);
    }

    public void addChild(String url) {
        Node node = this.network.addNode(url);
        if(!this.children.contains(node)) this.children.add(node);
    }

//    public boolean isValid() {
//        return (this.document != null);
//    }

    public boolean isIncludedInWebGraph() {
        return (this.isCollected && this.isValid && this.isProcessed());
    }

    public boolean isValid() {
        return this.isValid;
    }

    public boolean hasDocument() {
        return (this.document != null);
    }

    public boolean isProcessed() {
        return (this.children != null);
    }

    public boolean isAvailable() {return this.isAvailable;}

    public boolean isCollected() {
        return this.isCollected;
    }

    public void lock() {
        this.isAvailable = false;
    }

    public void unlock() {
        this.isAvailable = true;
    }

//    public void setValid(boolean isValid)
//    {
//        this.isValid = isValid;
//    }


    @Override
    public void run() {
        this.collectDocument();
        this.scrapeLinks();
    }

    public boolean collectDocument() {
        this.isCollected = true;
        try {
            Connection connection = Jsoup.connect(this.url);
            this.document = connection.get();
            log("Collected resource from " + this.url);
            this.network.numberOfValidNodes = this.network.numberOfValidNodes + 1;
            this.isValid = true;
            return hasDocument();

        } catch (IOException e) {
            log("Caught exception (IOException) on " + url + " --> " + e.getMessage());
            return false;
        }
    }

    public boolean scrapeLinks() {
        if (!isValid()) return false;
        this.children = new LinkedList<>();
        try {
            Elements parsedLinks = this.document.select("a[href]");

            for (Element link : parsedLinks) {
//                if (this.network.allNodes.size() >= this.network.maxNodes) break;
//                if (this.network.numberOfValidNodes >= this.network.maxNodes) break;
                addChild(link.absUrl("href"));
            }
//            log("Scraped " + this.children.size() + " links from " + this.url);
            log("Scraped " + this.children.size() + " links from " + this.url + " -- TOTAL: " + network.allNodes.size() + " -- NV: " + network.numberOfValidNodes);
            return isProcessed();
        } catch (Exception e) {
            log("Caught exception (" + e.getClass().getCanonicalName() + ")");
            return false;
        }
    }

    private void log(String x) {
        System.out.println("[N" + Thread.currentThread().getId() + "]: " + x);
    }

}
