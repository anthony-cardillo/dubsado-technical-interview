import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Crawler {

    private static final String helpString = "INVALID ARGUMENTS: java -jar WebCrawler.jar <max-nodes> <seed-url> <output-path> -- ARGS PROVIDED=";

    public static void main(String[] args) {
        int maxNodes = 50;
        String seedURL = "https://google.com";
        String path = "/";
        boolean showGraph = false;
        Network network = new Network();
        try {
            if (args.length == 3) {
                maxNodes = Integer.valueOf(args[0]);
                seedURL = args[1];
                path = args[2];

                Scanner s = new Scanner(System.in);
                System.out.print("Display Graph (true/false)? ");
                showGraph = s.nextBoolean();

                network.crawl(seedURL, maxNodes, path, showGraph);
            } else {
                log(helpString + args.length);
            }
        } catch (Exception e) {
            log("Caught exception (" + e.getClass().getCanonicalName() + ")");
            log(helpString);
        }
    }

    private static void log(String x) {
        System.out.println("[C" + Thread.currentThread().getId() + "]: ");
    }

}
