import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;



public class Network implements Serializable {

    public int maxNodes = 20;
    public List<Node> allNodes =  new LinkedList<>();
    public int numberOfValidNodes = 0;

    private static final int THREAD_COUNT = 10;
    private int activeThreadCount = 0;

    public boolean isFull() {return numberOfValidNodes >= maxNodes;}

    public synchronized void incrementActiveThreadCount() {++this.activeThreadCount;}
    public synchronized void decrementActiveThreadCount() {--this.activeThreadCount;}

    public boolean isActive() {return this.activeThreadCount > 0;}

    public void crawl(String seedURL, int maxNodes, String path, boolean showGraph) {
        long t1 = System.nanoTime();
        this.maxNodes = maxNodes;
        addNode(seedURL);

        try {

            log("STARING UP THREADS");
            List<DocumentCollector> collectors = new LinkedList<>();
            List<LinkScraper> scrapers = new LinkedList<>();


            for (int i = 0; i < THREAD_COUNT; i++) {

                DocumentCollector dc = new DocumentCollector(this);
                Thread dcThread = new Thread(dc);
                collectors.add(dc);

                LinkScraper ls = new LinkScraper(this);
                Thread lsThread = new Thread(ls);
                scrapers.add(ls);

                dcThread.start();
                lsThread.start();
            }

            while (!isActive()); // wait for threads to begin

            while (isActive()) {

                if (collectors.stream().filter(c -> !c.hasNodes()).findFirst().isPresent()) {
                    Node n1 = getNodeDC();
                    if (n1 != null) {
                        n1.lock();
                        collectors.stream().filter(c -> !c.hasNodes()).findFirst().get().queueNode(n1);
                    }
                }

                if (scrapers.stream().filter(c -> !c.hasNodes()).findFirst().isPresent()) {
                    Node n2 = getNodeLS();
                    if (n2 != null) {
                        n2.lock();
                        scrapers.stream().filter(c -> !c.hasNodes()).findFirst().get().queueNode(n2);
                    }
                }
                Thread.yield();
            }
            log("CLOSED ALL THREADS");

        } catch (
                Exception e) {
            log("Caught exception (" + e.getClass().getCanonicalName() + ") --> SHUTTING DOWN");
            e.printStackTrace();
        } finally {
            long t2 = System.nanoTime();
            double elapsedTime =  (double) (t2 - t1)/1_000_000_000.0;
            computeHITS(5);
            computeInOutDegree();
            computePageRank(30,0.85,1.0e-6);
            writeToFile(path);
            log("CRAWL COMPLETE --> Results saved to file @ " + path);
            log("NUMBER OF VALID NODES = " + filterNodes(allNodes).size());
            log("COMPLETED IN " + elapsedTime + " SECONDS");
            if(showGraph) {
                WebGraph wg = new WebGraph(this);
            }
        }

    }

    public synchronized Node addNode(String url) {
        Node node;
        List<String> urls = this.allNodes.stream().map(Node::getURL).collect(Collectors.toList());
        if(urls.contains(url)) {
            node = this.allNodes.stream().filter(n -> n.getURL().equals(url)).findFirst().get();
        } else {
            node = new Node(url, this);
            this.allNodes.add(node);
        }
        return node;
    }

    public synchronized Node getNodeDC() {
        for(Iterator<Node> iterator = this.allNodes.iterator(); iterator.hasNext();) {
            Node n = iterator.next();
            if(n.isAvailable() && !n.isCollected() && !n.isValid()) return n;
        }
        return null;
    }

    public synchronized Node getNodeLS() {
        for(Iterator<Node> iterator = this.allNodes.iterator(); iterator.hasNext();) {
            Node n = iterator.next();
            if(n.isAvailable() && n.isCollected() && n.isValid() && !n.isProcessed()) return n;
        }
        return null;
    }

    public List<Node> filterNodes(List<Node> nodes) {
        return nodes.stream().filter(n -> n.isIncludedInWebGraph()).collect(Collectors.toList());
    }

    public void computeHITS(int steps) {

        List<Node> G = filterNodes(this.allNodes);

        for (Node node : G) {
            node.authorityScore = 1;
            node.hubScore = 1;
        }

        for (int i = 0; i < steps; i++) {

            double norm = 0.0;
            for (Node node : G) {
                node.authorityScore = 0.0;
                for (Node parent : filterNodes(node.getParents()))
                    node.authorityScore += parent.hubScore;
                norm += Math.pow(node.authorityScore, 2);
            }
            norm = Math.sqrt(norm);

            for (Node node : G)
                node.authorityScore = node.authorityScore / norm;

            norm = 0.0;
            for (Node node : G) {
                node.hubScore = 0.0;
                for (Node child : filterNodes(node.getChildren()))
                    node.hubScore += child.authorityScore;
                norm += Math.pow(node.hubScore, 2);
            }
            norm = Math.sqrt(norm);

            for (Node node : G)
                node.hubScore = node.hubScore / norm;
        }

        for (Node node : G) {
            log("auth = " + node.authorityScore + " , hub = " + node.hubScore + " @ " + node.getURL());
        }
    }

    public void computePageRank(int steps, double d, double eps) {

        List<Node> G = filterNodes(this.allNodes);
        int N =  G.size();

        Dictionary<Node,Double> last =  new Hashtable<>();

        for(Node node: G) {
            node.pageRank = 1.0/N;
        }

        double dampingFactor = (1 - d) / N;

        for(int i = 0; i < steps; i++) {

            for(Node node : G) {
                last.put(node,node.pageRank);
            }

            for(Node node : G) {
                node.pageRank = dampingFactor;
                for (Node parent : filterNodes(node.getParents())) {
                    node.pageRank += (d * last.get(parent) / filterNodes(parent.getChildren()).size());
                }
            }

            boolean done = true;
            for(Node node : G) {
                double diff = Math.abs(node.pageRank - last.get(node)) / node.pageRank;
                if (diff > eps) {
                    done = false;
                    break;
                }
            }
            if(done) break;
        }

        for (Node node : G) {
            log("PR = " + node.pageRank + " @ " + node.getURL());
        }

    }

    public void computeInOutDegree() {
        List<Node> G = filterNodes(this.allNodes);
        for(Node node: G) {
            node.inDegree = filterNodes(node.getParents()).size();
            node.outDegree = filterNodes(node.getChildren()).size();
            log("inDeg = " + node.inDegree + " , outDeg = " + node.outDegree + " @ " + node.getURL());
        }
    }

    private static void log(String x) {
        System.out.println("[N" + Thread.currentThread().getId() + "]: " + x);
    }

    private void writeToFile(String path) {
        try {
            FileOutputStream fout = new FileOutputStream(path);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
