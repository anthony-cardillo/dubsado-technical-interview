import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.graph.implementations.SingleGraph;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class WebGraph {

    WebGraph(Network network) {

        Graph graph = new MultiGraph("Web Graph");

        List<Node> nodes = network.allNodes.stream().filter(n -> n.isValid() && n.isProcessed()).collect(Collectors.toList());

        try {
            for (Node node : nodes) {
                org.graphstream.graph.Node n = graph.addNode(node.getURL());
                n.addAttribute("ui.label", n.getId());
                n.removeAttribute("ui.hide");
            }

            try {
                for (Node parent : nodes) {
                    List<Node> children = parent.getChildren().stream().filter(n -> nodes.contains(n)).collect(Collectors.toList());
                    for (Node child : children) {
                        String id = parent.getURL() + " --> " + child.getURL();
                        graph.addEdge(id, parent.getURL(), child.getURL(), true);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                graph.display();
                String path = "/Users/anthonycardillo/Documents/MESc/course-work/9668-internet-algorithms/project/WebCrawler";
                path = path + "/fig/graph.png";
                graph.addAttribute("ui.screenshot", path);
            }
        } catch (Exception e) {
            System.out.println("GRAPH STREAM EXCEPTION --> TOO MANY NODES TO DISPLAY");
        }

    }
}
