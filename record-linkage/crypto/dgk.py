import codecs
import gmpy2
import json
import os


# multiplication & mod function using gmpy2 
def mulmod(x, y, n):
	return gmpy2.t_mod(gmpy2.mul(x, y), n)

# increment function using gmpy2 
def inc(x):
	return gmpy2.add(x, 1)

# decrement function using gmpy2 
def dec(x):
	return gmpy2.sub(x, 1)

# quadratic residuosity test based on Euler's criterion and the Legendre symbol
def quadratic_residuosity(x, p):
	return 0 if gmpy2.legendre(x, p) == 1 else 1


class DGK():

	# Constructor
	def __init__(self, public_key_file, private_key_file):

		# using sagemath generated key files from A.Essex package (dgk-keygen.sage)
		self.public_key = self.import_public_key(public_key_file)
		self.private_key = self.import_private_key(private_key_file) 

		# precompute powers of h --> blinding elements h^r for 
		# to conserve memory we on precompute h^r for r = [1,2,4,8 ... 2^(l-1)]
		self.bpowers_of_h = self.precomp_bpowers_of_h(self.public_key['h'], self.public_key['l'], self.public_key['n'])
		
		# precompute powers of g --> message elements g^m 
		# for threshold protocol 0 <= m <= d
		self.powers_of_g = self.precomp_powers_of_g(self.public_key['g'], self.public_key['s'], self.public_key['n'])

		# precompute ALL quadratic residues of Z_s	
		self.quadratic_residues = self.precomp_quadratic_residues(self.public_key['s'])

		# Precompute log_m(g^um) for faster decryption
		# Precompute log_s(g^m) for faster decryption
		self.log_gm = self.precompute_discrete_log(self.public_key['s'], self.private_key['u'], self.private_key['p'], self.powers_of_g)
		
		# create random number generator with 16 byte seed value
		self.RNG = RandomNumberGenerator(16) 


	# ----------------------------------------------------------
	# initialization functions -  used only by constructor
	# ----------------------------------------------------------

	def import_public_key(self, filepath):

		public_key_param = ['n', 'l', 's', 'g', 'h', 'd', 'f']

		try:
			with open(filepath, 'r') as f:
				public_key = json.load(f)
		except:
			print('failed to load public key file')

		for k in public_key:
			public_key[k] = gmpy2.mpz(public_key[k]) 

		for k in public_key_param:
			if(k not in public_key):
				raise Exception('missing public key parameters')

		if not (gmpy2.is_prime(public_key['s'])):
			raise Exception('message space s is not prime')

		if (gmpy2.bit_length(public_key['s']) > 20):
			raise Exception('message space is too large for precomputation')

		if (gmpy2.powmod(public_key['g'], public_key['s'], public_key['n']) != 1):
			raise Exception('generator g does not have order s')
		
		return public_key

	def import_private_key(self, filepath):

		private_key_param = ['p', 'u']

		try:
			with open(filepath, 'r') as f:
				private_key = json.load(f)
		except:
			print('failed to load private key file')

		for k in private_key:
			private_key[k] = gmpy2.mpz(private_key[k]) 

		for k in private_key_param:
			if(k not in private_key):
				raise Exception('missing public key parameters')


		if not (gmpy2.is_prime(private_key['p'])):
			raise Exception('p is not prime')

		# q = gmpy2.mpz(gmpy2.div(self.public_key['n'], private_key['p']))
		q = gmpy2.mpz(self.public_key['n'] // private_key['p'])

		if not (gmpy2.is_prime(q)):
			raise Exception('q = n/p is not prime')

		if (gmpy2.powmod(self.public_key['h'], private_key['u'], private_key['p']) != 1):
			raise Exception('Generator h has wrong order modulo p')

		return private_key

	def precomp_bpowers_of_h(self, h, l, n):
		bpowers = [h]
		for i in range(l):
			bpowers.append(gmpy2.powmod(bpowers[i - 1], 2, n))

		return bpowers

	def precomp_powers_of_g(self, g, s, n):
		powers = [1]
		for i in range(1, s):
			powers.append(mulmod(powers[i - 1], g, n)) 

		return powers 

	def precomp_quadratic_residues(self, s):
		return [0] + [quadratic_residuosity(i, s) for i in range(1, s)]

	def precompute_discrete_log(self, s, u, p, gm):
		log_gm = {}
		for i in range(s):
			# key = (g^i)^(u mods) modp --> val = i
			log_gm[str(gmpy2.powmod(gm[i], gmpy2.t_mod(u, s), p))] = i

		return log_gm


	# ----------------------------------------------------------
	# core functions 
	# ----------------------------------------------------------

	def randh(self):
		hr = 1
		for i in range(self.public_key['l']):
			if self.RNG.rand(2) != 0:
				hr = mulmod(hr, self.bpowers_of_h[i], self.public_key['n'])
		return hr

	def encrypt(self, m):
		gm = self.powers_of_g[gmpy2.t_mod(m, self.public_key['s'])]
		hr = self.randh()
		return mulmod(gm, hr, self.public_key['n'])

	def decrypt(self, c):
		gm = gmpy2.powmod(c, self.private_key['u'], self.private_key['p'])
		return self.log_gm[str(gm)]

	# Rerandomize ciphertext
	def rerandomize(self, c):
		return mulmod(c, self.encrypt(0), self.public_key['n'])

	# ----------------------------------------------------------
	# homomorphic functions & threshold evaluation
	# ----------------------------------------------------------

	# homomorphically sum two encrypted plaintexts c1, c2
	def hadd(self, c1, c2):
		return mulmod(c1, c2, self.public_key['n'])

	# homomorphically scale encrypted plaintext c by visible scalar m
	def hscale(self, c, m):
		return gmpy2.powmod(c, m, self.public_key['n'])

	def blind_qr(self, c):
		bf = self.RNG.rand_in_range(1, dec(self.public_key['s'])) # select random blinding factor from message space
		bf = gmpy2.powmod(bf, 2, self.public_key['s'])	# square it to preserve quadratic residuosity
		return gmpy2.powmod(c, bf, self.public_key['n'])

	def threshold(self, c, t):
		offset = self.public_key['f'] + self.public_key['d'] - 1
		c = self.hadd(c, self.encrypt(offset))
		c = self.hadd(c, t)
		c = self.blind_qr(c)

		return self.quadratic_residues[self.decrypt(c)]



# interface to gmpy2 random number generator
class RandomNumberGenerator():

	# Constructor
	def __init__(self, byte_size):
		seed = int.from_bytes(os.urandom(byte_size), 'big')	# cryptographically secure RNG seed
		self.state = gmpy2.random_state(seed)

	# returns random int between 0 and upper_bound - 1
	def rand(self, upper_bound):
		return gmpy2.mpz_random(self.state, upper_bound)

	# returns random int between min_val and max_val
	def rand_in_range(self, min_val, max_val):
		return gmpy2.add(gmpy2.mpz_random(self.state, gmpy2.sub(inc(max_val), min_val)), min_val)

	# returns random int between 0 and 
	def rand_bits(self, bit_length):
		return gmpy2.mpz_urandomb(self.state, bit_length)
