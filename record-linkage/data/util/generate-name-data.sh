#!/bin/bash
# [description] creates a list of names from Ohio voter data
# [usage] data/util/generate-name-data.sh <voter-file>.txt

path=$1
dir="data/names/"
file=$(basename $path)

# LC_ALL variable allows cut to ignore encording scheme and bypass illegal byte sequence errors 
LC_ALL=C cut -d ',' -f 4,5 $path > $dir$file

