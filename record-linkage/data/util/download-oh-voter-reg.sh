#!/bin/bash
# [description] downloads the latest version of the Ohio voter registry
# [usage] data/util/download-oh-voter-reg.sh

# base url - OH SOS website
url="https://www6.sos.state.oh.us/ords/f?p=VOTERFTP:DOWNLOAD::FILE:NO:2:P2_PRODUCT_NUMBER:"

# create a directory for county voter files
dir="data/oh-voter-reg/"
date=$(date '+%Y-%m-%d')
mkdir $dir$date/

ext=".txt"

# download the voter registry file of each county
echo "downloading county files ..."
counties=$(cut -d, -f1 ${dir}oh-counties.txt)
for i in $counties; do
    file=$dir$date/$i$ext
    wget -q -O $file --no-check-certificate $url$i
    echo "  -  $date/$i$ext"
done

# merge the county files into a single state wide voter registry
echo "creating master dataset from county files ..."
head -n 1 $dir$date/1$ext > $dir$date$ext # headers
tail -q -n +2 $dir$date/* >> $dir$date$ext
echo "  -  $date$ext"

# compress county files and delete download directory
echo "archiving county files ..."
zip -qr $dir$date.zip $dir$date/*
echo "  -  $dir$date.zip"

# delete county files
rm $dir$date/*
rmdir $dir$date
