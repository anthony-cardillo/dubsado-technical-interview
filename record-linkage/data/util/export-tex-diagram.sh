#!/bin/bash
# [description] exports PDFs and PNGs of LaTeX diagrams
# [usage] util/export-tex-diagram.sh <diagram-file>.tex

path=$1
dir=$(dirname $path)
file=$(basename $path | cut -f 1 -d '.')

# compile PDF from LaTeX file
pdflatex -interaction batchmode -output-directory $dir $path

# delete auxillary files from pdflatex cmd
rm $dir/$file.aux
rm $dir/$file.log


# convert PDF to PNG
convert -density 600 $dir/$file.pdf -quality 90 $dir/$file.png
sleep 5

# delete auxillary files from convert cmd
# rm $dir/$file.synctex.gz