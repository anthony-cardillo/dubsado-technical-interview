--------------------------------------------------
NAME STATISTICS
--------------------------------------------------

RECORD SIZES
  - full database: 7674144
  - first names: 206922
  - last names: 364419

AVERAGE STRING LENGTHS
  - first names: 7
  - last names: 8
