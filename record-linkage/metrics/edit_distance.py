# -----------------------------------------------------------
# EDIT DISTANCE
# -----------------------------------------------------------

def lcs_length_recursive(a, b):

    if len(a) == 0 or len(b) == 0:
        return 0

    aa = a[:-1]    
    bb = b[:-1]

    if a[-1] == b[-1]:  # if last elements of x and y are equal
        return lcs_length_recursive(aa, bb) + 1
    else:
        return max(lcs_length_recursive(aa, b), lcs_length_recursive(a, bb))

def lcs_length_dynamic(a, b):

    m = len(a)
    n = len(b)

    d = [[None for j in range (n + 1)] for i in range(m + 1)]  

    for i in range(m + 1):
        d[i][0] = 0

    for j in range(n + 1):
        d[0][j] = 0

    for i in range(1, m+1):
        for j in range(1, n+1):
            if a[i-1] == b[j-1]:
                d[i][j] = d[i - 1][j - 1] + 1
            else:
                d[i][j] = max(d[i - 1][j], d[i][j - 1])

    return d[-1][-1]

def lcs_dist(a, b):
    lcs_length = lcs_length_dynamic(a, b)
    return len(a) + len(b) - (2 * lcs_length)

def lcs_sim(a, b):
    lcs_length = lcs_length_dynamic(a, b)
    return (2 * lcs_length) / (len(a) + len(b))

def norm_lcs(a, b):
    lcs_length = lcs_length_dynamic(a, b)
    # return 1 - (2 * lcs_length) / (len(a) + len(b))
    return (2 * lcs_length) / (len(a) + len(b))

def levenshtein_recursive(a, b):

    if len(a) == 0 or len(b) == 0:
        return 0

    aa = x[:-1]      
    bb = y[:-1]

    d_ins = levenshtein_recursive(aa, b) + 1
    d_del = levenshtein_recursive(a, bb) + 1

    if a[-1] == b[-1]:
        d_sub = levenshtein_recursive(aa,bb)
    else:
        d_sub = levenshtein_recursive(aa,bb) + 1

    return min(d_ins, d_del, d_sub)

def levenshtein_dynamic(a, b):

    m = len(a)
    n = len(b)

    d = [[None for j in range (n + 1)] for i in range(m + 1)]  

    for i in range(m + 1):
        d[i][0] = i

    for j in range(n + 1):
        d[0][j] = j
    
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            d_del = d[i - 1][j] + 1
            d_ins = d[i][j - 1] + 1
            d_sub = d[i - 1][j - 1] + (0 if a[i - 1] == b[j - 1] else 1)

            d[i][j] = min(d_ins, d_del, d_sub)

    return d[-1][-1]
    
def norm_levenshtein(a, b):
    levenshtein = levenshtein_dynamic(a, b)
    return 1 - (levenshtein / max(len(a),len(b)))
