# -----------------------------------------------------------
# JARO DISTANCE
# -----------------------------------------------------------

def jaro(a, b):

    # some quick cases - should not be requirement of secure protocol with assumption of proper usage
    if len(a) == 0 and len(b) == 0:
        return 1 
    elif len(a) == 0 or len(b) == 0:
        return 0


    # PROCESS 1 -  COUNT MATCHES WITH DISTANCE
    matches = 0
    d = match_distance(len(a), len(b))

    return d

    index_a = [] # match indices for a
    index_b = [] # match indices for b

    for i in range(len(a)):
        window = range(max(0, i - d), min(len(b), i + d + 1))
        for j in window:
            if j in index_b:
                continue
            if a[i] == b[j]:
                index_a.append(i)
                index_b.append(j)
                break

    index_a.sort()
    index_b.sort()

    match_a = ''.join([a[i] for i in index_a])
    match_b = ''.join([b[i] for i in index_b])

    m = len(match_a)

    # quick case to speed things up
    if m == 0:
        return 0

    # PROCESS 2 - COUNT TRANSPOSITIONS
    trans = [match_a[i] == match_b[i] for i in range(m)]
    t = trans.count(False) / 2


    jaro_distance = (float(m) / len(a)) + (float(m) / len(b)) + ((float(m - t) / m))
    jaro_distance = jaro_distance / 3

    return jaro_distance


def match_distance(len_a, len_b):
    return (max(len_a, len_b) // 2) - 1