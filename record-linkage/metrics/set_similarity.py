# -----------------------------------------------------------
# SET SIMILARITY METRICS
# -----------------------------------------------------------

def ngrams(x, n=2):
    pad = " " * (n - 1)
    x = pad + x + pad
    return [x[i:i+n] for i in range(0,len(x) - n + 1)]

def set_intersection(a, b):
    return list(set(a) & set(b))

def jaccard(a, b, n=2):
    a_ng = ngrams(a, n)
    b_ng = ngrams(b, n)
    a_ins_B = set_intersection(a_ng, b_ng)
    return len(a_ins_B) / (len(a_ng) + len(b_ng) - len(a_ins_B))

def overlap(a, b, n=2):
    a_ng = ngrams(a, n)
    b_ng = ngrams(b, n)
    a_ins_B = set_intersection(a_ng, b_ng)
    return len(a_ins_B) / (min(len(a_ng),len(b_ng)))

def dice(a, b, n=2):
    a_ng = ngrams(a, n)
    b_ng = ngrams(b, n)
    a_ins_B = set_intersection(a_ng, b_ng)
    return (2 * len(a_ins_B)) / (len(a_ng) + len(b_ng))

def tversky(a, b, alpha=1.0, beta=1.0, n=2):
    # default parameters give Jaccard index
    a_ng = ngrams(a, n)
    b_ng = ngrams(b, n)
    a_ins_B = set_intersection(a_ng, b_ng)
    return len(a_ins_B) / (len(a_ins_B) + alpha * (len(a_ng) - len(a_ins_B)) + beta * (len(b_ng) - len(a_ins_B)))

# UNIGRAM SIMILARITY - quick implementations
def ug_jaccard(a, b): return jaccard(a, b, 1)
def ug_overlap(a, b): return overlap(a, b, 1)
def ug_dice(a, b): return dice(a, b, 1)

# BIGRAM SIMILARITY - quick implementations
def bg_jaccard(a, b): return jaccard(a, b, 2)
def bg_overlap(a, b): return overlap(a, b, 2)
def bg_dice(a, b): return dice(a, b, 2)

# TRIGRAM SIMILARITY - quick implementations
def tg_jaccard(a, b): return jaccard(a, b, 3)
def tg_overlap(a, b): return overlap(a, b, 3)
def tg_dice(a, b): return dice(a, b, 3)
