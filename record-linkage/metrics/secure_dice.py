import math
import random 
import hashlib
import string

import set_similarity as ss
import numpy as np

from crypto.dgk import DGK 

# -----------------------------------------------------------
# BLOOM FILTER DICE CODE
# -----------------------------------------------------------
def bloom_score(a, b, l, k):
    a_bf = bloom_filter(a, l, k)
    b_bf = bloom_filter(b, l, k)
    return (2 * bit_intersection(a_bf, b_bf)) / (sum(a_bf) + sum(b_bf))

def bloom_filter(s, l=200, k=5, h1=hashlib.sha1, h2=hashlib.md5):
    bg = ss.ngrams(s, 2)
    bf = [0 for i in range(0,l)]
    for x in bg:
        print(x)
        for i in range(0,k):
            g = (int(h1(x.encode()).hexdigest(),16) + (i * int(h2(x.encode()).hexdigest(),16))) % l
            bf[g] = 1
            print(g)
    return bf

def bit_intersection(a, b):
    return sum([a[i] and b[i] for i in range(0,len(a))])


# PARAMATERIZED IMPLEMENTATIONS OF BFE DICE METRIC

# 200-bit bloom filters
def bloom_200_5(s, t): return bloom_score(s, t, 200, 5)
def bloom_200_10(s, t): return bloom_score(s, t, 200, 10)
def bloom_200_25(s, t): return bloom_score(s, t, 200, 25)
def bloom_200_50(s, t): return bloom_score(s, t, 200, 50)

# 500-bit bloom filters
def bloom_500_5(s, t): return bloom_score(s, t, 500, 5)
def bloom_500_10(s, t): return bloom_score(s, t, 500, 10)
def bloom_500_25(s, t): return bloom_score(s, t, 500, 25)
def bloom_500_50(s, t): return bloom_score(s, t, 500, 50)

# 1000-bit bloom filters
def bloom_1000_5(s, t): return bloom_score(s, t, 1000, 5)
def bloom_1000_10(s, t): return bloom_score(s, t, 1000, 10)
def bloom_1000_25(s, t): return bloom_score(s, t, 1000, 25)
def bloom_1000_50(s, t): return bloom_score(s, t, 1000, 50)


# -----------------------------------------------------------
# HOMOMORPHIC ENCRYPTION / LITTLE FUNCTION BASED DICE
# -----------------------------------------------------------

def he_score(a, b, t, t_table=None, bg_list=None, dgk=None):

    if t_table == None:
        t_table = threshold_table(20, 0.01)

    if bg_list == None:
        bg_list = bigram_list()

    if dgk == None:
        dgk = DGK('crypto/keys/dgk-128-public.json', 'crypto/keys/dgk-128-private.json')

    a_bg = encoded_bigrams(a, bg_list)
    b_bg = [dgk.encrypt(x) for x in encoded_bigrams(b, bg_list)]

    # set_cardinality_threshold = t_table[t][len(a)][len(b)]
    sct_list = [dgk.encrypt(sct) for sct in t_table[t][len(b)]]
    set_cardinality_threshold = dgk.hscale(sct_list[len(a)], -1) # negative value so it may be homomorphically subtacted from QR offset

    set_cardinality = dgk.encrypt(0) 
    for i in range(len(a_bg)):
        if a_bg[i] == 1:
            set_cardinality = dgk.hadd(set_cardinality, b_bg[i])

    return dgk.threshold(set_cardinality, set_cardinality_threshold)

# CARDINALITY THRESHOLD
def threshold_table(maxSize, res):
    table = {}
    for t in list(np.arange(0.0, 1.0 + res , res)):
        table[t] = [[cardinality_threshold(i, j, t) for j in range(maxSize + 1)] for i in range(maxSize + 1)] 

    return table

def cardinality_threshold(len_a, len_b, t):
    thresh = math.ceil(t * (len_a + len_b) / 2)
    return thresh if (thresh <= len_a and thresh <= len_b) else min(len_a, len_b) + 1

# BIGRAM ENCODING FUNTIONS
def encoded_bigrams(x, bg_list):
    x_bg = ss.ngrams(x, 2)

    if bg_list == None:
        bg_list = bigram_list()

    return[(1 if bg in x_bg else 0) for bg in bg_list]

def bigram_list():
    alphabet = list(string.ascii_lowercase)
    bigrams = []
    for i in alphabet:
        bigrams.append(" " + i)
        bigrams.append(i + " ")

        for j in alphabet:
            bigrams.append(i + j)
            
    return list(set(bigrams))