import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/..'

import sys
sys.path.append(dir_path)
sys.path.insert(1, 'crypto/')
sys.path.insert(2, 'metrics/')

import string

from functools import reduce

from crypto.dgk import DGK
from metrics import jaro_distance as jd

# -----------------------------------------------------------
# HOMOMORPHIC ENCRYPTION / LITTLE FUNCTION BASED JARO
# -----------------------------------------------------------

def he_score(a, b, t=0.5, d_table=None, dgk=None):

    maxSize = 20

    if d_table == None:
        d_table = distance_table(maxSize)

    if dgk == None:
        # dgk = DGK('crypto/keys/dgk-128-public.json', 'crypto/keys/dgk-128-private.json')
        dgk = DGK('../crypto/keys/dgk-128-public.json', '../crypto/keys/dgk-128-private.json')

    # PROCESS 1 - DECIDE MATCH DISTANCE (HIDING LENGTHS)
    d_list = [dgk.encrypt(x) for x in d_table[len(a)]]          # A encrypts all possible distances given its length
    d = d_list[len(b)]                                          # B selects the match distance corresponding to its length
    d = dgk.decrypt(d)                                          # A decrypts to reveal the match distance

    # PROCESS 2 - COUNT MATCHES WITH DISTANCE

    # A generates encrpyted list of windows encoding characters present as [1]
    a_win = [[dgk.encrypt(x) for x in w] for w in windows(a, maxSize, d)]

    # B selects the value correspoding to its own character in each window and homomorphically sums selections to get number of matches
    m = dgk.encrypt(0)                                                          
    for x in [a_win[i][string.ascii_lowercase.find(b[i])] for i in range(len(b))]:
        m = dgk.hadd(m, x)
    
    # A decrypts to reveal the match count
    print(dgk.decrypt(m))      

    # PROCESS 3 - COUNT TRANSPOSITIONS



    # PROCESS 4 - EVALUATE THRESHOLD


    return dgk.decrypt(m)


def distance_table(maxSize):
    return [[jd.match_distance(i, j) for j in range(maxSize + 1)] for i in range(maxSize + 1)]  

def windows(s, depth, dist=0):
    alpha = string.ascii_lowercase
    matrix = [[0 for x in alpha] for i in range(depth)]
    for i in range(len(s) + dist):
        for c in s[max(0, i - dist): min(len(s), i + dist) + 1]:
            matrix[i][alpha.find(c)] = 1

    return matrix

def indices

def str_from_window(window):
    alpha = string.ascii_lowercase
    x = ''
    for i in range(len(string.ascii_lowercase)):
        if window[i] == 1:
            x += alpha[i]

    return x
        