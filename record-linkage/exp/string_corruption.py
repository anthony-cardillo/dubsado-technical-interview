import string
import random

from geco.phonetic import Phonetic 
from geco.ocr import OCR

# -----------------------------------------------------------
# UTILITY METHODS
# -----------------------------------------------------------

# POSITION FUNCTION - returns uniformly random index from input string
def uniform_positon(x, n=1):
    if x == '':
        return [0]

    elif n == 1:
        return [random.randint(0, len(x) - 1)]

    elif n <= len(x):
        return random.sample(range(0, len(x) - 1), n)

    else:
        return -1 # not elgible for mod


def single_uniform_position(x): return uniform_positon(x,1)[0]

# EDIT CHARACTER FUNCTION - returns random character
def rand_char():
    char_set = string.ascii_lowercase #lowercase alphabet
    return random.choice(char_set)


# -----------------------------------------------------------
# FUNDAMENTAL ERROR MODELS
# -----------------------------------------------------------

def insertion(x, n=1):
    mod_positions = uniform_positon(x + ' ', n)
    for i in mod_positions:
        x = x[:i] + rand_char() + x[i:]
    return x

def deletion(x, n=1):
    mod_positions = uniform_positon(x, n)
    for i in mod_positions:
        x = x[:i] + x[i+1:]
    return x

def substitution(x, n=1):
    mod_positions = uniform_positon(x, n)
    for i in mod_positions:
        x = x[:i] + rand_char() + x[i+1:]
    return x

def transposition(x, n=1):
    mod_positions = uniform_positon(x[:-1], n)
    try:

        for i in mod_positions:
            y = x[:i] + x[i+1] + x[i] + ('' if i == len(x) - 2  else x[i+2:])
        return y

    except Exception as e:
        print('Transposition error model failed --> ' + e.__class__.__name__)
        print('string: ' + x)
        print('length: ' + str(len(x)))
        print('modifier position: ' + str(i))
    

# SCE - single character edit (quick implementations)
def sce_insertion(x): return insertion(x, 1)
def sce_deletion(x): return deletion(x, 1)
def sce_substitution(x): return substitution(x, 1)
def sce_transposition(x): return transposition(x, 1)

# DCE - double character edit (quick implementations)
def dce_insertion(x): return insertion(x, 2)
def dce_deletion(x): return deletion(x, 2)
def dce_substitution(x): return substitution(x, 2)
def dce_transposition(x): return transposition(x, 2)

# TCE - triple character edit (quick implementations)
def tce_insertion(x): return insertion(x, 3)
def tce_deletion(x): return deletion(x, 3)
def tce_substitution(x): return substitution(x, 3)
def tce_transposition(x): return transposition(x, 3)



# -----------------------------------------------------------
# PRACTICAL ERROR MODELS - SCE only
# -----------------------------------------------------------

# ROW KEYBOARD EDIT 
def keyboard_row(x, error_model=None):
    if error_model == None:
        error_model = row_model()

    i = uniform_positon(x, 1)[0]
    return x[:i] + random.choice(error_model[x[i]]) + x[i+1:]

def row_model():
    return {'a':'s', 'b':'vn', 'c':'xv', 'd':'sf', 'e':'wr', 'f':'dg', 'g':'fh', 'h':'gj', 'i':'uo', 'j':'hk', 'k':'jl', 'l':'k', 'm':'n', 'n':'bm', 'o':'ip', 'p':'o', 'q':'w', 'r':'et', 's':'ad', 't':'ry', 'u':'yi', 'v':'cb', 'w':'qe', 'x':'zc', 'y':'tu', 'z':'x'}

# COLUMN KEYBOARD EDIT 
def keyboard_col(x, error_model=None):
    if error_model == None:
        error_model = col_model()

    i = uniform_positon(x, 1)[0]
    return x[:i] + random.choice(error_model[x[i]]) + x[i+1:]

def col_model():
    return {'a':'qzw','b':'gh','c':'df','d':'erc','e':'ds34','f':'rvc', 'g':'tbv', 'h':'ybn', 'i':'k89', 'j':'umn','k':'im', 'l':'o', 'm':'jk', 'n':'hj', 'o':'l90', 'p':'0', 'q':'a12', 'r':'f45', 's':'wxz', 't':'g56', 'u':'j78', 'v':'fg', 'w':'s23', 'x':'sd', 'y':'h67', 'z':'as'}

# COMIBINED KEYBOARD MODEL   
def keyboard(x, row_col_ratio=0.5, row=None, col=None):
    return keyboard_row(x, row) if (random.random() <= row_col_ratio) else keyboard_col(x, col)
    
# OPTICAL CHARACTER RECOGNITION (OCR) MODEL
def ocr(x, error_model=None):
    if error_model == None:
        error_model = ocr_model()
    
    return error_model.corrupt_value(x)

def ocr_model():
    return OCR(lookup_file_name='exp/geco/ocr-edits.csv', has_header_line=False, unicode_encoding='ascii', position_function=single_uniform_position)

# PHONETIC ERROR MODEL
def phonetic(x, error_model=None):
    if error_model == None:
        error_model = phonetic_model()

    return error_model.corrupt_value(x)

def phonetic_model():
    return Phonetic(lookup_file_name='exp/geco/phonetic-edits.csv', has_header_line=False, unicode_encoding='ascii')