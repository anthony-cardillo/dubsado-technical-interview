from __future__ import division

import random 
import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from datetime import datetime, timedelta

# -----------------------------------------------------
# CLASSIFIER DEFINITION
# -----------------------------------------------------

def match(a, b, metric, threshold):
    score = metric(a, b)
    return 1 if score >= threshold else 0


# -----------------------------------------------------
# EXPERIMENT
# -----------------------------------------------------

def run_positive_condition_experiment(records, cor_alg, metric, number_of_trials=100, res=0.01, filepath=''):

    # record the parameters of the experiment for plotting
    results = {}
    results['test_condition'] = 'positive'
    results['cor_alg'] = cor_alg.__name__
    results['metric'] = metric.__name__.replace('_','-')
    results['number_of_trials'] = number_of_trials
    results['res'] = res
    
    log('running ' + results['test_condition'] + ' condition experiment on ' + results['metric'] + '() --> ' + str(number_of_trials) + ' rounds with ' + results['cor_alg'] + ' error model ...')

    results['threshold'] = list(np.arange(0.0, 1.0 + res , res))
    results['positive_rate'] = [] 
    results['negative_rate'] = []

    for t in results['threshold']:
        inputs = random.sample(set(records), number_of_trials)
        trials = [match(x, cor_alg(x), metric, t) for x in inputs]
        results['positive_rate'].append(positive_rate(trials, number_of_trials))
        results['negative_rate'].append(negative_rate(trials, number_of_trials))

    log('COMPLETE --> '  + str(len(results['threshold'])) + ' results recorded')
    
    if filepath is not '':
        save_results(results, filepath)

    return results

def run_negative_condition_experiment(records, metric, number_of_trials=100, res=0.01, filepath=''):

    # record the parameters of the experiment for plotting
    results = {}
    results['test_condition'] = 'negative'
    results['cor_alg'] = ''
    results['metric'] = metric.__name__.replace('_','-')
    results['number_of_trials'] = number_of_trials
    results['res'] = res
    
    log('running ' + results['test_condition'] + ' condition experiment on ' + results['metric'] + '() --> ' + str(number_of_trials) + ' rounds ...')

    # results['threshold'] = list(np.arange(0.3, 1 + res, res))
    results['threshold'] = list(np.arange(0.0 , 1.0 + res, res))
    results['positive_rate'] = [] 
    results['negative_rate'] = []

    for t in results['threshold']:
        sampled_records = random.sample(set(records), number_of_trials * 2)
        inputs = zip(sampled_records[0::2], sampled_records[1::2])
        trials = [match(x[0], x[1], metric, t) for x in inputs]
        results['positive_rate'].append(positive_rate(trials, number_of_trials))
        results['negative_rate'].append(negative_rate(trials, number_of_trials))

    log('COMPLETE --> '  + str(len(results['threshold'])) + ' results recorded')
    
    if filepath is not '':
        save_results(results, filepath)

    return results

# positive condition measurements
def positive_rate(trials, number_of_trials):
    return (sum(trials) / number_of_trials)

def negative_rate(trials, number_of_trials):
    return (number_of_trials - sum(trials) ) / number_of_trials


# -----------------------------------------------------
# IMPORT/PREPROCESSING METHODS
# -----------------------------------------------------

def import_name_data(filepath='data/names/2019-11-09.txt'):
    log('importing names (LAST, FIRST) from ' + filepath + ' ...')

    try:
        names = [line.rstrip('\n').replace('"', '').split(",") for line in open(filepath)]
        names.pop(0)
        
        log('COMPLETE --> ' + str(len(names)) + ' names recorded')
        return names

    except Exception as e:
        log('import failed --> ' + e.__class__.__name__)

def get_first_names(names):
    first_names = [name[1] for name in names]
    return apply_sim_criteria(first_names)

def get_last_names(names):
    last_names = [name[0] for name in names]
    return apply_sim_criteria(last_names) 

def apply_sim_criteria(names):
    names = del_duplicates(names)                   # delete duplicates
    names = [x for x in names if len(x) > 1]        # remove abbreviated names
    names = [x.replace(' ', '') for x in names]     # remove spaces
    names = [x for x in names if x.isalpha()]       # remove names with special characters
    names = [x.lower() for x in names]              # make lowercase
    return names

def del_duplicates(names):
    return list(set(names))

def sample_list(names):
    return random.choice(names)


# -----------------------------------------------------
# UTILITY FUNCTIONS
# -----------------------------------------------------
def save_results(data, filepath):
    log('saving results to ' +  filepath + ' ...')
    with open(filepath, 'wb') as f:
        pickle.dump(data, f)
    log('COMPLETE')

def log(msg, blocked=False):
    if blocked:
        return
    log = '[' + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ']: '
    log += msg
    print(log)


## ^^ MOVE THESE FUNCTIONS TO util.py LATER