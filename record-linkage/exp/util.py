import random 
import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def load_results(filepath):
    with open(filepath, 'rb') as f:
        return pickle.load(f)

def format_results(data, result_key):
    col = ['threshold', result_key]
    return {key:data[key] for key in col if key in data}

def log(msg, blocked=False):
    if blocked:
        return
    log = '[' + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ']: '
    log += msg
    print(log)

def log_work(filepath='hist.txt'):
    readline.write_history_file(filepath)

def plot_results(results, labels=None, filepath=''):

    # change formatting parameters for plotting
    plt.rc('font',**{'family':'serif','sans-serif':['Computer Modern Roman']})
    plt.rc('text', usetex=True)

    plt.rc('grid', linestyle='dashed', linewidth=0.5, color='black')


    if not all(r['test_condition'] == results[0]['test_condition'] for r in results):
        raise Exception('missmatched experiment types')

    if labels == None:
        raise Exception('missing label dictionary')

    test_condition = results[0]['test_condition']

    y_labels = {'positive':'False Negative Rate', 'negative':'False Positve Rate'}
    y_label = y_labels[test_condition]

    leg_locs = {'positive':'upper left', 'negative':'upper right'}
    leg_loc = leg_locs[test_condition]

    key_map = {'positive':'negative_rate', 'negative':'positive_rate'}
    key = key_map[test_condition]

    # create figure
    fig, ax = plt.subplots()

    for r in results:
        data = format_results(r, key)
        if test_condition == 'negative':
            data['threshold'].pop(0)
            data[key].pop(0)

        df = pd.DataFrame.from_dict(data)
        df.plot(x='threshold', y=key, label=labels[r['metric']], ax=ax, linewidth=1, marker='o', markersize=2)


    plt.grid(True)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    plt.ylabel(y_label)
    plt.ylim((0.0,1.01))

    plt.xlabel('Match Threshold $t$')
    plt.xlim((0.0, 1.01))

    legend = ax.legend(loc=leg_loc, fancybox=False, frameon=True, framealpha=1)
    legend.get_frame().set_linewidth(0.5)
    legend.get_frame().set_edgecolor('black')
    legend.get_frame().set_facecolor('white')

    if filepath == '':
        plt.show()
    else:
        plt.savefig(filepath,format='pdf', bbox_inches='tight')
        plt.close(fig)

def plot_result(r, filepath=''):

    test_condition = r['test_condition']

    key_map = {'positive':'negative_rate', 'negative':'positive_rate'}
    key = key_map[test_condition]

    # create figure
    fig, ax = plt.subplots()

    data = format_results(r, key)
    if test_condition == 'negative':
        data['threshold'].pop(0)
        data[key].pop(0)

    df = pd.DataFrame.from_dict(data)
    df.plot(x='threshold', y=key, ax=ax, linewidth=1, marker='o', markersize=2)

    plt.rc('grid', linestyle='dashed', linewidth=0.5, color='black')
    plt.grid(True)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    plt.ylim((0.0,1.01))
    plt.xlim((0.0, 1.01))

    if filepath == '':
        plt.show()
    else:
        plt.savefig(filepath,format='pdf', bbox_inches='tight')
        plt.close(fig)