#!/usr/bin/env python3
import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/..'

import sys
sys.path.append(dir_path)

import hypothesis_testing as sim

from scipy.stats import norm
import matplotlib.pyplot as plt
import numpy as np

out_dir = 'data/results/name-statistics/'
stat_fp = out_dir + 'stat.txt'
first_hist_fp = out_dir + 'first-name-length-distribution.pdf'
last_hist_fp = out_dir + 'last-name-length-distribution.pdf'

# import/create datasets
db = sim.import_name_data('data/names/2019-11-09.txt')
first_names = sim.get_first_names(db)
last_names = sim.get_last_names(db)

# count number of records in each
size_full = len(db)
size_first = len(first_names)
size_last = len(last_names)

# get the length of each entry
lens_first = list(map(len, first_names))
lens_last = list(map(len, last_names))

# calculate the average length
avg_len_first = int(round( sum(lens_first) / size_first ))
avg_len_last = int(round( sum(lens_last) / size_last ))

#write observed statistics to a file
with open(stat_fp, "w") as stat_file:

    stat_file.write('-'*50 +'\n')
    stat_file.write('NAME STATISTICS\n')
    stat_file.write('-'*50 +'\n')

    stat_file.write('\nRECORD SIZES\n')
    stat_file.write('  - full database: ' + str(size_full) + '\n')
    stat_file.write('  - first names: ' + str(size_first) + '\n')
    stat_file.write('  - last names: ' + str(size_last) + '\n')


    stat_file.write('\nAVERAGE STRING LENGTHS\n')
    stat_file.write('  - first names: ' + str(avg_len_first)  + '\n')
    stat_file.write('  - last names: ' + str(avg_len_last) + '\n')


# plot distribution of lengths
def plot_hist(lens, filepath='', label_x=0, label_y=0):

    bins = list(set(lens))
    bins.sort()

    labels = list(map(str, bins))
    
    fig, ax = plt.subplots()
    plt.hist(lens, density=True, bins=bins, edgecolor='black', linewidth=1, alpha=0.7, color='b', zorder=3)
    
    bin_w = (max(bins) - min(bins)) / (len(bins) - 1)
    plt.xticks(np.arange(min(bins)+bin_w/2, max(bins), bin_w), bins)
    plt.xlim(bins[0], bins[-1])

    # fit normal distribution to plot
    mu, std = norm.fit(lens)
    xmin, xmax = plt.xlim()
    x = np.linspace(xmin, xmax, 100)
    p = norm.pdf(x, mu, std)
    plt.plot(x, p, 'k', linewidth=1, zorder=4)

    fit_label = '$\; \mu = ' + str(round(mu, 2)) + ', \; \sigma = ' + str(round(std,2)) + '\;$'
    ax.text(label_x, label_y, fit_label, bbox={'facecolor': 'white', 'alpha': 1, 'pad': 7})

    plt.rc('font',**{'family':'serif','serif':['Computer Modern Roman']})
    plt.rc('text', usetex=True)

    ax.yaxis.grid(linestyle='dashed', linewidth=0.5, color='black', zorder=0)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    plt.ylabel('Probability')
    plt.xlabel('String Length')

    if filepath is '':
        plt.show()
    else:
        plt.savefig(filepath,format='pdf', bbox_inches='tight')
        plt.close(fig)

plot_hist(lens_first, first_hist_fp)
plot_hist(lens_first, first_hist_fp, 10, 0.1725)
plot_hist(lens_last, last_hist_fp, 12, 0.1225)




