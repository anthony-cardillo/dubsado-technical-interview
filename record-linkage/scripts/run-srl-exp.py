#!/usr/bin/env python3

import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/..'

import sys
sys.path.append(dir_path)
sys.path.insert(1, 'exp/')
sys.path.insert(2, 'metrics/')
sys.path.insert(3, 'exp/geco/')
sys.path.insert(4, 'crypto/')


from exp import hypothesis_testing as sim
from exp import string_corruption as err 

from metrics import secure_dice as dice
# from metrics import secure_jaro as jaro

from crypto.dgk import DGK

out_dir = 'data/results/secure-record-linkage/'

metrics = [
    dice.bloom_score,
    dice.he_score
    # jaro.he_score
]

number_of_trials = 100000
res = 0.01

# create tables once to speed up matching
t_table = dice.threshold_table(20, res)
bg_list = dice.bigram_list()
dgk = DGK('crypto/keys/dgk-128-public.json', 'crypto/keys/dgk-128-private.json')


print(dice.he_score('aleks', 'alex', 0.5, t_table, bg_list, dgk))

# create models to speed practical error modeling tests
# phonetic_model = err.phonetic_model()
# def phonetic(x): return err.phonetic(x, phonetic_model)

# ocr_model = err.ocr_model()
# def ocr(x): return err.ocr(x, ocr_model)

# row_model = err.row_model()
# col_model = err.col_model()
# def keyboard(x): return err.keyboard(x, 0.5, row_model, col_model)