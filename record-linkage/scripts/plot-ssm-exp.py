#!/usr/bin/env python3

import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/..'

import sys
sys.path.append(dir_path)
sys.path.insert(1, 'exp/')

import matplotlib.pyplot as plt
import pandas as pd
import time

from exp import hypothesis_testing as sim
from exp import util


result_dir = 'data/results/approximate-string-matching/'
figure_dir = 'fig/approximate-string-matching/set-sim-'

labels = {
        # 'ug-dice':'Dice Coeff. (n = 1)',
        # 'ug-jaccard':'Jaccard Index (n = 1)',
        # 'ug-overlap':'Overlap Coeff. (n = 1)',
        'bg-dice':'Dice Coeff. (n = 2)',
        # 'bg-jaccard':'Jaccard Index (n = 2)',
        'bg-overlap':'Overlap Coeff. (n = 2)',
        # 'tg-dice':'Dice Coeff. (n = 3)',
        # 'tg-jaccard':'Jaccard Index (n = 3)',
        # 'tg-overlap':'Overlap Coeff. (n = 3)'
}

experiment_types = [
    # 'fnr-sce_insertion',
    # 'fnr-sce_deletion',
    # 'fnr-sce_substitution',
    # 'fnr-sce_transposition',
    # 'fnr-keyboard',
    # 'fnr-ocr',
    'fnr-phonetic'
    # 'fpr'
]

result_ext = '.dat'
figure_ext = '.pdf'

for exp_type in experiment_types:

    result_files = [result_dir + x.replace('-','_') + '-' + exp_type + result_ext for x in labels]
    results = [util.load_results(fp) for fp in result_files]

    figure_fp = figure_dir + exp_type + figure_ext 

    util.plot_results(results, labels, figure_fp)
    util.plot_results(results, labels, figure_fp)