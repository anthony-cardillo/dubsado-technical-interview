#!/usr/bin/env python3
import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/..'

import sys
sys.path.append(dir_path)
sys.path.insert(1, 'metrics/')

import hypothesis_testing as sim
import string_corruption as err  
from metrics import set_similarity as scr 
             

import matplotlib.pyplot as plt
import pandas as pd
import time

from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

out_dir = 'data/results/trial-size-study/'
pos_fig = out_dir + 'pos.pdf'
neg_fig = out_dir + 'neg.pdf'

pos_exps = []
neg_exps = []

exp_complete = False

# -----------------------------
# RUN SIMULATION
# -----------------------------
if not exp_complete:

    names = sim.import_name_data('data/names/2019-11-09.txt')
    last_names = sim.get_last_names(names)

    for i in range(2,6):
        size = 10 ** i

        pos_filepath = out_dir + 'fnr-' + str(size) + '.dat'
        pos_exps.append(sim.run_positive_condition_experiment(last_names, err.insertion, scr.dice, size, 0.01, pos_filepath))
        
        neg_filepath = out_dir + 'fpr-' + str(size) + '.dat'
        neg_exps.append(sim.run_negative_condition_experiment(last_names, scr.dice, size, 0.01, neg_filepath))

# -----------------------------
# LOAD PREV. SIMULATION
# -----------------------------
else:

    for i in range(2,6):
        size = 10 ** i

        pos_filepath = out_dir + 'fnr-' + str(size) + '.dat'
        pos_exps.append(sim.load_results(pos_filepath))

        neg_filepath = out_dir + 'fpr-' + str(size) + '.dat'
        neg_exps.append(sim.load_results(neg_filepath))


def plot_figure(results, filepath=''):

    if not all(r['test_condition'] == results[0]['test_condition'] for r in results):
        log('ERROR --> missmatched experiment types')
        return

    test_condition = results[0]['test_condition']

    y_labels = {'positive':'False Negative Rate', 'negative':'False Positve Rate'}
    y_label = y_labels[test_condition]

    leg_locs = {'positive':'lower left', 'negative':'lower right'}
    leg_loc = leg_locs[test_condition]

    key_map = {'positive':'negative_rate', 'negative':'positive_rate'}
    key = key_map[test_condition]

    # create figure
    fig, ax = plt.subplots()
    for r in results:
        data = sim.format_results(r, key)
        df = pd.DataFrame.from_dict(data)
        label = "$N = " + str(r['number_of_trials']) + "$"
        df.plot(x='threshold', y=key, label= label, ax=ax, linewidth=1, marker='o', markersize=2)

    plt.rc('font',**{'family':'serif','serif':['Computer Modern Roman']})
    plt.rc('text', usetex=True)

    plt.rc('grid', linestyle='dashed', linewidth=0.5, color='black')
    plt.grid(True)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    plt.ylabel(y_label)
    plt.ylim((0.0,1.01))

    plt.xlabel('Match Threshold $t$')
    plt.xlim((0.0, 1.01))

    legend = ax.legend(loc=leg_loc, fancybox=False, frameon=True, framealpha=1)
    legend.get_frame().set_linewidth(0.5)
    legend.get_frame().set_edgecolor('black')
    legend.get_frame().set_facecolor('white')
    
    if test_condition == 'positive':
        axins = zoomed_inset_axes(ax, 4.5, bbox_to_anchor=(.15, .3, .6, .5), bbox_transform=ax.transAxes, loc=3) 
        
        for r in results:
            axins.plot(r['threshold'], r[key], linewidth=0.75)

        x1, x2 = 0.65, 0.775
        axins.set_xlim(x1, x2) # apply the x-limits

        y1, y2 = -0.005, 0.12
        axins.set_ylim(y1, y2) # apply the y-limits

        mark_inset(ax, axins, loc1=3, loc2=1, fc="none", ec="0.5")

    elif test_condition == 'negative':
        axins = zoomed_inset_axes(ax, 4.5, bbox_to_anchor=(.25, .3, .6, .5), bbox_transform=ax.transAxes, loc=3)

        for r in results:
            axins.plot(r['threshold'], r[key], linewidth=0.75)

        x1, x2 = 0.15, 0.275
        axins.set_xlim(x1, x2) # apply the x-limits

        y1, y2 = .045, 0.17
        axins.set_ylim(y1, y2) # apply the y-limits

        mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5", zorder = -6)

    plt.tick_params(axis='x', which='both', direction='in', bottom=True, top=True, labelbottom=False)
    plt.tick_params(axis='y', which='both', direction='in', right=True, left=True, labelleft=False)
    plt.grid(True)

    if filepath is '':
        plt.show()
    else:
        plt.savefig(filepath,format='pdf', bbox_inches='tight')
        plt.close(fig)



plot_figure(pos_exps, filepath=pos_fig)
plot_figure(pos_exps, filepath=pos_fig)
plot_figure(neg_exps, filepath=neg_fig)


