#!/usr/bin/env python3

import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/..'

import sys
sys.path.append(dir_path)
sys.path.insert(1, 'exp/')
sys.path.insert(2, 'metrics/')
sys.path.insert(3, 'exp/geco/')

from exp import hypothesis_testing as sim
from exp import string_corruption as err 

from metrics import edit_distance as ed 
from metrics import jaro_distance as jd 

from exp import util

import matplotlib.pyplot as plt
import pandas as pd
import time

out_dir = 'data/results/temp/'

metric = ed.norm_lcs

# create models to speed practical error modeling tests
# phonetic_model = err.phonetic_model()
# def phonetic(x): return err.phonetic(x, phonetic_model)

# ocr_model = err.ocr_model()
# def ocr(x): return err.ocr(x, ocr_model)

# row_model = err.row_model()
# col_model = err.col_model()
# def keyboard(x): return err.keyboard(x, 0.5, row_model, col_model)

error_model = err.sce_insertion
# err.sce_insertion
# err.sce_deletion
# err.sce_substitution
# err.sce_transposition
# keyboard
# ocr
# phonetic

names = sim.import_name_data('data/names/2019-11-09.txt')
last_names = sim.get_last_names(names)

number_of_trials = 100
res = 0.01

exps = []

result_ext = '.dat'
figure_ext = '.pdf'

pos_fp = out_dir + str(metric.__name__) + '-fnr-' + str(error_model.__name__)
neg_fp = out_dir + str(metric.__name__) + '-fpr'

labels = {
        'ug-dice':'Dice Coeff. (n = 1)',
        'ug-jaccard':'Jaccard Index (n = 1)',
        'ug-overlap':'Overlap Coeff. (n = 1)',
        'bg-dice':'Dice Coeff. (n = 2)',
        'bg-jaccard':'Jaccard Index (n = 2)',
        'bg-overlap':'Overlap Coeff. (n = 2)',
        'tg-dice':'Dice Coeff. (n = 3)',
        'tg-jaccard':'Jaccard Index (n = 3)',
        'tg-overlap':'Overlap Coeff. (n = 3)'
}

# POSITIVE HYPOTHESIS TESTING
pos_res_fp = pos_fp + result_ext
pos_result = sim.run_positive_condition_experiment(last_names, error_model, metric, number_of_trials, res, pos_res_fp)
# pos_result = util.load_results(pos_res_fp)

pos_fig_fp =  pos_fp + figure_ext
util.plot_result(pos_result, pos_fig_fp)


# NEGATIVE HYPOTHESIS TESTING
neg_res_fp = neg_fp + result_ext
neg_result = sim.run_negative_condition_experiment(last_names, metric, number_of_trials, res, neg_res_fp)
# pos_result = util.load_results(neg_res_fp)

neg_fig_fp =  neg_fp + figure_ext
util.plot_result(neg_result, neg_fig_fp)