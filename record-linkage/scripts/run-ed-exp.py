#!/usr/bin/env python3

import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/..'

import sys
sys.path.append(dir_path)
sys.path.insert(1, 'exp/')
sys.path.insert(2, 'metrics/')
sys.path.insert(3, 'exp/geco/')

from exp import hypothesis_testing as sim
from exp import string_corruption as err 

from metrics import edit_distance as ed 
from metrics import jaro_distance as jd 

import matplotlib.pyplot as plt
import pandas as pd
import time

out_dir = 'data/results/approximate-string-matching/'
# out_dir = 'data/results/temp/'

metrics = [
    ed.norm_lcs
    # ed.norm_lcs,
    # ed.norm_levenshtein,
    # jd.jaro
]

# create models to speed practical error modeling tests
phonetic_model = err.phonetic_model()
def phonetic(x): return err.phonetic(x, phonetic_model)

ocr_model = err.ocr_model()
def ocr(x): return err.ocr(x, ocr_model)

row_model = err.row_model()
col_model = err.col_model()
def keyboard(x): return err.keyboard(x, 0.5, row_model, col_model)

error_models = [
    err.sce_insertion,
    err.sce_deletion,
    err.sce_substitution,
    err.sce_transposition,
    keyboard,
    ocr
    # phonetic
]

names = sim.import_name_data('data/names/2019-11-09.txt')
last_names = sim.get_last_names(names)

number_of_trials = 100000
res = 0.01

exps = []
for metric in metrics:

    # POSITIVE HYPOTHESIS TESTING
    for error_model in error_models:
        pos_filepath = out_dir + str(metric.__name__) + '-fnr-' + str(error_model.__name__) + '.dat'
        exps.append(sim.run_positive_condition_experiment(last_names, error_model, metric, number_of_trials, res, pos_filepath))

    # # POSITIVE HYPOTHESIS TESTING
    neg_filepath = out_dir + str(metric.__name__) + '-fpr.dat'
    exps.append(sim.run_negative_condition_experiment(last_names, metric, number_of_trials, res, neg_filepath))

 