#!/usr/bin/env python3

import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/..'

import sys
sys.path.append(dir_path)
sys.path.insert(1, 'exp/')
sys.path.insert(2, 'metrics/')
sys.path.insert(3, 'exp/geco/')

from metrics import secure_dice as sd
from metrics import edit_distance as ed 


print('EX - Transformations on the Longest Common Subsequence (LCS):\n')

a1 = 'ABCDE'
print('a = ' + a1 + '  |a| = ' + str(len(a1)))

b1 = 'CDEFG'
print('b = ' + b1 + '  |b| = ' + str(len(b1)) + '\n')

len_lcs_rec = ed.lcs_length_recursive(a1, b1)
print('LenLCS(a,b) = ' + str(len_lcs_rec) + '  ~ by recursive algorithm')

len_lcs_dyn = ed.lcs_length_recursive(a1, b1)
print('LenLCS(a,b) = ' + str(len_lcs_dyn) + '  ~ by dynamic algorithm\n')

lcs_dist = ed.lcs_dist(a1, b1)
print('LCSDist(a,b) = ' + str(lcs_dist))

lcs_sim = ed.lcs_sim(a1, b1)
print('LCSSim(a,b) = ' + str(lcs_sim) + '\n')


print('EX - LCS Distance and Simililarity:\n')

a2 = 'CRYPTO'
print('a = ' + a2 + '  |a| = ' + str(len(a2)))

b2 = 'KRYPTO'
print('b = ' + b2 + '  |b| = ' + str(len(b2)) + '\n')

lcs_sim = ed.lcs_sim(a2, b2)
print('LCSSim(a,b) = ' + str(lcs_sim))

lcs_dist = ed.lcs_dist(a2, b2)
print('LCSSim(a,b) = ' + str(lcs_dist) + '\n')




# print(sd.bloom_filter(a,24,2))
# print('\n')
# print(sd.bloom_filter(b,24,2))



