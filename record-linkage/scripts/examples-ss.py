#!/usr/bin/env python3

import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/..'

import sys
sys.path.append(dir_path)
sys.path.insert(1, 'exp/')
sys.path.insert(2, 'metrics/')
sys.path.insert(3, 'exp/geco/')

from metrics import secure_dice as sd
from metrics import set_similarity as ss

print('EX - Set Similarity Metrics on Bigram Decompositions:\n')

a = 'CRYPTO'
print('a = ' + a + '  |a| = ' + str(len(a)))

b = 'KRYPTO'
print('b = ' + b + '  |b| = ' + str(len(b)) + '\n')


A = ss.ngrams(a, 2)
print('A = ' + str(A) + '  |A| = ' + str(len(A)))

B = ss.ngrams(b, 2)
print('B = ' + str(B) + '  |B| = ' + str(len(B)) + '\n')


score_1 = ss.bg_jaccard(a,b)
print('JACCARD(A,B) = ' + str(score_1))

score_2 = ss.bg_overlap(a,b)
print('OVERLAP(A,B) = ' + str(score_2))

score_3 = ss.bg_dice(a,b)
print('DICE(A,B) = ' + str(score_3) + '\n')

# print(sd.bloom_filter(a,24,2))
# print('\n')
# print(sd.bloom_filter(b,24,2))
