#!/usr/bin/env python3

import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/..'

import sys
sys.path.append(dir_path)
sys.path.insert(1, 'exp/')

import matplotlib.pyplot as plt
import pandas as pd
import time

from exp import hypothesis_testing as sim
from exp import util


result_dir = 'data/results/approximate-string-matching/'
figure_dir = 'fig/ngrams/'

labels = {
    'ug':'n = 1',
    'bg':'n = 2',
    'tg':'n = 3'
}

metrics = [
    'jaccard',
    'overlap',
    'dice'
]

experiment_types = [
    # 'fnr-sce_insertion',
    # 'fnr-sce_deletion',
    # 'fnr-sce_substitution',
    # 'fnr-sce_transposition',
    'fnr-keyboard',
    # 'fnr-ocr',
    # 'fnr-phonetic'
    'fpr'
]

result_ext = '.dat'
figure_ext = '.pdf'


for metric in metrics:

    for exp_type in experiment_types:

        result_files = [result_dir + x + '_' + metric + '-' + exp_type + result_ext for x in labels]
        results = [util.load_results(fp) for fp in result_files]

        figure_fp = figure_dir + metric + '-' + exp_type + figure_ext 

        lbls = {k+'-'+metric: v for k, v in labels.items()}

        util.plot_results(results, lbls, figure_fp)
        util.plot_results(results, lbls, figure_fp)
