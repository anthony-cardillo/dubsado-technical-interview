#!/usr/bin/env python3

import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + '/..'

import sys
sys.path.append(dir_path)
sys.path.insert(1, 'exp/')

import matplotlib.pyplot as plt
import pandas as pd
import time

from exp import hypothesis_testing as sim
from exp import util


result_dir = 'data/results/approximate-string-matching/'
figure_dir = 'fig/approximate-string-matching/phonetic/'

labels = {
        'bg-dice':'Dice',
        'bg-jaccard':'Jaccard',
        'bg-overlap':'Overlap',
        'norm-lcs':'LCS',
        'norm-levenshtein':'Levenshtein',
        'jaro':'Jaro'
}

exp_type = 'fnr-phonetic'

result_ext = '.dat'
figure_ext = '.pdf'


result_files = [result_dir + x.replace('-','_') + '-' + exp_type + result_ext for x in labels]
results = [util.load_results(fp) for fp in result_files]

figure_fp = figure_dir + exp_type + figure_ext 

util.plot_results(results, labels, figure_fp)
util.plot_results(results, labels, figure_fp)